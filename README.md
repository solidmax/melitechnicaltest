#MUTANT DETECTOR

Ayuda a Magneto a detectar que humanos pertenecen a los mutantes a través de sus secuencias de adn.

##Instalación

Para garantizar el fuincionamiento montar sobre la plataforma Google App Engine utilizando una base de datos Postgresql.

Clonar el proyecto sobre GCloud
```
git clone https://solidmax@bitbucket.org/solidmax/melitechnicaltest.git
```
Cambiar las variables de entorno en el archivo app.yaml para que apunten a tu base de datos postgre

Luego en la carpeta del proyecto crearlo usando 
```
gcloud app create
```
Y luego ejecutarlo 
```
gcloud app deploy app.yaml --project meli-mutant-test
```     
Se instalarán automaticamente los requerimientos para que funcione y el sitio estará listo para su uso.

##USO

Mutant detector se encuentra alojado en: https://meli-mutant-test.appspot.com/
Para hacer uso del detector de mutantes se debe usar un programa como Postman o similar que permita enviar solicitudes HTTP POST, el programa recibirá archivos json que contengan una matriz NxN, usando el formato
{
"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
Esta debe ser enviada a la url: https://meli-mutant-test.appspot.com/mutant/

Si el adn es mutante se enviará el mensaje de respuesta HTTP-200 OK, en caso de ser humano se enviará 403-Forbidden.

Ademas se puede solicitar el conteo de humanos vs mutantes en: https://meli-mutant-test.appspot.com/stats
esta puede ser realizada a través de cualquier navegador web.

##Mejoras futuras
La base de datos creada almacena la cadena de adn (dna_sequence) y si esta corresponde a un mutante o no: 
La cadena es almacenada con el formato: 
["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
Como futura optimización se sugiere crear solo una gran cadena de letras eliminando comas, llaves y comillas, pero almacenando en un nuevo campo el largo de cada cadena en caso de que se quisiera reconstruir esta. 
