# [START gae_python37_app]
import os
import json
from flask import Flask, request
import psycopg2

db_user = os.environ.get('CLOUD_SQL_USERNAME')
db_password = os.environ.get('CLOUD_SQL_PASSWORD')
db_name = os.environ.get('CLOUD_SQL_DATABASE_NAME')
db_connection_name = os.environ.get('CLOUD_SQL_CONNECTION_NAME')

# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = Flask(__name__)


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return "Welcome, please send a json mutant dna through POST to /mutant to check it, to check stats visit /stats"


@app.route('/stats')
def stats():
    result = get_stats()
    return str(result)


@app.route('/mutant/', methods=['POST'])
def check_if_mutant():
    global mutant_chains
    data = request.get_json()
    is_mutante = is_mutant(data)
    insert_into_bd(data, is_mutante)

    # return show
    if is_mutante:
        return "HTTP 200-OK"
    else:
        return "403-Forbidden"


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END gae_python37_app]

CHAIN_LENGTH = 4
mutant_chains = 0


def get_db_cnx():
    # When deployed to App Engine, the `GAE_ENV` environment variable will be
    # set to `standard`
    if os.environ.get('GAE_ENV') == 'standard':
        # If deployed, use the local socket interface for accessing Cloud SQL
        host = '/cloudsql/{}'.format(db_connection_name)
    else:
        # If running locally, use the TCP connections instead
        # Set up Cloud SQL Proxy (cloud.google.com/sql/docs/mysql/sql-proxy)
        # so that your application can use 127.0.0.1:3306 to connect to your
        # Cloud SQL instance
        host = '127.0.0.1'
    cnx = psycopg2.connect(dbname=db_name, user=db_user,
                           password=db_password, host=host)
    return cnx


def insert_into_bd(new_dna_sequence, new_bool):
    cnx = get_db_cnx()
    with cnx.cursor() as cursor:
        new_dna_sequence = json.dumps(new_dna_sequence['dna'])
        query = """insert into dna_entries (dna_sequence,is_mutant) values('{}',{}) ON CONFLICT DO NOTHING;""".format(str(new_dna_sequence), new_bool)
        cursor.execute(query)
        # result = cursor.fetchall()
        # result = "success!"

    cnx.commit()
    cnx.close()
    # return result


def get_stats():
    cnx = get_db_cnx()
    with cnx.cursor() as cursor:
        # get total count
        query = """select count(*) from dna_entries;"""
        cursor.execute(query)
        total_dnas = cursor.fetchall()
        # get mutant count 
        cursor.execute('SELECT count(CASE WHEN is_mutant THEN 1 END) FROM dna_entries;')
        mutant_dnas = cursor.fetchall()
    cnx.commit()
    cnx.close()

    json_result = generate_stat_json(total_dnas[0][0], mutant_dnas[0][0])
    return str(json_result)


def generate_stat_json(total_dnas, mutant_dnas):
    total_dnas = int(total_dnas)
    mutant_dnas = int(mutant_dnas)
    human_dnas = total_dnas - mutant_dnas
    if human_dnas == 0:
        ratio = mutant_dnas
    else:
        ratio = mutant_dnas/human_dnas
    dna_json = {
        "count_mutant_dna": mutant_dnas,
        "count_human_dna": human_dnas,
        "ratio": ratio
    }
    return json.dumps(dna_json)


def is_mutant(dna_matrix):
    global mutant_chains
    mutant_chains = 0
    dna_length = int(len(dna_matrix['dna'][0]))
    print(dna_matrix)

    # analize data
    analize_horizontal(dna_matrix, dna_length)
    analize_vertical(dna_matrix, dna_length)
    analize_diagonal(dna_matrix, dna_length)
    return determine_if_dna_is_mutant()


def update_chain_counter(current_genes):
    global mutant_chains
    current_genes += 1
    # print(" equal genes: " + str(current_genes))
    if current_genes == 4:
        print("mutant chain founded!")
        mutant_chains += 1
        current_genes = 0
    return current_genes


def determine_if_dna_is_mutant():
    print("mutant chains founded: " + str(mutant_chains))
    if mutant_chains >= 2:
        return True
    else:
        return False


def analize_horizontal(dna_matrix, dna_length):
    print("analizing horizontal: ")
    for i in range(dna_length):
        current_nitrogen = dna_matrix['dna'][i][0]
        equal_genes = 0
        for j in range(dna_length):
            # print(str(i) + " " + str(j) + "= " + str(dna_matrix[i][j]))
            if dna_matrix['dna'][i][j] == current_nitrogen:
                equal_genes = update_chain_counter(equal_genes)
            else:
                equal_genes = 1
            current_nitrogen = dna_matrix['dna'][i][j]


def analize_vertical(dna_matrix, dna_length):
    print("analizing vertical: ")
    for j in range(dna_length):
        current_nitrogen = dna_matrix['dna'][0][j]
        equal_genes = 0
        for i in range(dna_length):
            # print(str(i) + " " + str(j) + "= " + str(dna_matrix[i][j]))
            if dna_matrix['dna'][i][j] == current_nitrogen:
                equal_genes = update_chain_counter(equal_genes)
            else:
                equal_genes = 1

            current_nitrogen = dna_matrix['dna'][i][j]


def analize_diagonal(dna_matrix, dna_length):
    print("analizing diagonal: ")
    for i in range(dna_length):
        for j in range(dna_length):
            current_nitrogen = dna_matrix['dna'][i][j]
            equal_genes = 0

            for k in range(CHAIN_LENGTH):
                if (i + k) < dna_length and (j + k) < dna_length:
                    # print("checking " + str(i+k) + " " + str(j+k))
                    if dna_matrix['dna'][i + k][j + k] == current_nitrogen:
                        equal_genes = update_chain_counter(equal_genes)
                    else:
                        equal_genes = 1
                    current_nitrogen = dna_matrix['dna'][i + k][j + k]